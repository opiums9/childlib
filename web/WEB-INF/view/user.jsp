<%-- 
    Document   : user
    Created on : 08.12.2023, 20:04:49
    Author     : root
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<body>
		<div id="pagewrap">

			<!-- Site content -->
			<div id="content">
                            <div id="about" class="page">
                                <c:forEach var="libs" items="${libs}">
                                    <c:if test="${libs.id == book_id}">
                                        <h1>${libs.name}</h1>
                                        <p>${libs.desc}</p>
                                        <br>
                                        <c:forEach var="contents" items="${contents}">
                                            <c:if test="${contents.id == book_id}">
                                                <p>${contents.content}</p>
                                            </c:if>
                                        </c:forEach>
                                    </c:if>
                                </c:forEach>
                            </div>
			</div>
