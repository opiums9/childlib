<%-- 
    Document   : index
    Created on : 08.12.2023, 17:56:08
    Author     : root
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

	<body>
		<div id="pagewrap">
			<!-- Site header and navigation -->
			<div id="header"><a href="/"><img src="images/logo-null.png" alt="" /></a>
				<ul id="navigation">
					<li><a class="about" href="#about"><img src="images/icon-about.png" alt="Главная" /></a></li>
					<li><a class="work" href="#work"><img src="images/icon-work.png" alt="Книги" /></a></li>
					<li><a class="contact" href="#contact"><img src="images/icon-contact.png" alt="Контакты" /></a></li>
				</ul>
			</div>
			
			<!-- Site content -->
			<div id="content">
				<div id="about" class="page">
					<div class="left">
						<img src="images/logo.png" alt="Лого" />
					</div>
					<div class="right">
						<h1>Чтение — вот лучшее учение!</h1>
						<p>
						&#10004; Наша детская библиотека предоставляет обширный выбор книг</br>
						&#10004; Мы уделяем особое внимание образовательному аспекту</br>
						&#10004; Мы обеспечиваем высокий уровень безопасности для детей в онлайн-среде</br>
						&#10004; Наша библиотека доступна в любое время и из любого места</br>
						&#10004; Коллекция книг в библиотеке охватывает разные возрастные группы</br>
						&#10004; Мы поощряем общение и обмен идеями среди наших читателей</br>
						&#10004; Вы всегда можете поделиться нашей библиотекой с друзьями и близкими
						</p>
						<br>
						<ul>
							<li><p align="center"><img src="images/1.png" width="68" alt="Читайте" />Читайте</p></li>
							<li><p align="center"><img src="images/2.png" width="68" alt="Учитесь" />Учитесь</p></li>
							<li><p align="center"><img src="images/3.png" width="68" alt="Собирайте" />Собирайте</p></li>
							<li><p align="center"><img src="images/4.png" width="68" alt="Делитесь" />Делитесь</p></li>
						</ul>
					</div>
				</div>

				<div id="work" class="page">
					<div class="left">
						<div class="card">
							<img src="images/books/1.jpg" />
						</div>
					</div>
					<div class="right">
						<ul id="work01">
							<li><a href="user?id=1"><img src="images/books/1.jpg" /></a></li>
							<li><a href="user?id=2"><img src="images/books/2.jpg" /></a></li>
							<li><a href="user?id=3"><img src="images/books/3.jpg" /></a></li>
							<li><a href="user?id=4"><img src="images/books/4.jpg" /></a></li>
							
							<li><a href="user?id=5"><img src="images/books/5.jpg" /></a></li>
							<li><a href="user?id=6"><img src="images/books/6.jpg" /></a></li>
							<li><a href="user?id=7"><img src="images/books/7.jpg" /></a></li>
							<li><a href="user?id=8"><img src="images/books/8.jpg" /></a></li>
							
							<li><a href="user?id=9"><img src="images/books/9.jpg" /></a></li>
							<li><a href="user?id=10"><img src="images/books/10.jpg" /></a></li>
							<li><a href="user?id=11"><img src="images/books/11.jpg" /></a></li>
							<li><a href="user?id=12"><img src="images/books/12.jpg" /></a></li>
						</ul>
					</div>
				</div>
				<div id="contact" class="page">
					<div class="left">
					</br><b3>&nbsp;&nbsp;&nbsp;Написать письмо:</b3>
						<form id="mail" action="" method="post">
							<ul>
								<li class="msgname"><label for="Name">Имя: </label><input class="input" name="name" type="text" required /></li>
								<li><label for="Email">Email: </label><input class="input" name="email" type="email" required /></li>
								<li><label for="Message">Сообщение: </label><textarea name="body" cols="1" rows="5" required></textarea></li>
								<li><input id="submit" value="Отправить" type="submit" /></li>
							</ul>
						</form>
					</div>
					<div class="right">
						<b3>Наши контакты</b3>
						<div id="contactinfo">
							<p>Email: <a href="mailto:admin@opiums.ru">admin@opiums.ru</a></p>
							<p>Адрес: г. Томск, ул. Учебная, 48Д</p>
							<p>Телефон: <a href="tel:+7(3822)1234567890">+7(3822)1234567890</a></p>
						</div>
						<br>
						<br>
						<br>
						<br>
						<br>
						<b3>Теги:</b3>
						<p><font color="gray">детская библиотека, children's library</font></p>
					</div>
				</div>
			</div>
			